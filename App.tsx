import React, { useEffect } from 'react';
import { SafeAreaView, Text } from 'react-native';
import messaging from '@react-native-firebase/messaging'

const App = () => {
  const getToken = () => messaging().getToken();

  useEffect(() => {
    getToken().then(
      (token) => console.log('Token for direct push notification: ' + token)
    );
  });

  return (
    <SafeAreaView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>FIREBASE NOTIFICATIONS RECEIVER</Text>
    </SafeAreaView>
  );
};

export default App;
